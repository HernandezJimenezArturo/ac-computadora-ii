## Mapa de Computadoras Eléctronicas
```plantuml
@startmindmap
	*[#Turquoise] Computadoras eléctronicas
    **_ antes eran
    ***[#Turquoise] Computadoras electromecanicas
    ****[#white]: Una de las mas 
grandes construidas 
es la;
    *****[#Turquoise] Harvard Mark 1
    ******[#white] Eje de 15 m
    *******[#Turquoise]: atravesaba todas
     las estructuras;
    ********_ para
    *********[#white]:mantener en sincronía 
    el dispositivo;
    **********[#Turquoise]: utilizaba un motor 
    de 5 caballos;
    ******_ Construida por
    *******[#Turquoise] IBM
    ********[#white] En 1944
    *********[#Turquoise]: Durante la segunda guerra 
    mundial para los aliados;
    ******[#white] Usos
    *******[#Turquoise]: hacer simulación y calculos
     para el proyecto Manhattan;
    ********[#white] proyecto Manhattan
    *********[#Turquoise]: desarrolló la 
    bomba atomicá;
    *******[#Turquoise]: Hacer 3 sumas o
     restas por segundo;
    *******[#Turquoise]: Hacer una multiplicación 
    en 6 segundos;
    *******[#Turquoise]: Hacer una división
     en 15 segundos;
    ******_ tenía 
    *******[#white] 765000 componentes
    ******[#white] 80 km de cable
    ******[#white] tenia 3500 Relés
    *******[#Turquoise] fallaba un relé por día
    ********[#white]: El problema surgía al hacer
     problemas mas complejos;
    *******[#Turquoise] Relé
    ********_ es
    *********[#white]: Un interruptor mecanico 
    controlado electricamente;
    **********_ era
    ***********[#Turquoise]: la piedra angular de esas 
    computadoras electromagneticas;
    ********[#white] compuesta por
    *********[#Turquoise] Brazo de hierro
    **********_ Tiene
    ***********[#white] masa
    ************[#Turquoise] Entra en juego la inercia
    *************_ es
    **************[#white]: Posición que presentan los
     cuerpos al cambiar de su estado 
     en movimiento;
    *********[#Turquoise] Bobina
    **********[#white]: Alambre de cobre 
    aislado y enrollado;
    *********[#Turquoise] Dos contactos
    ********[#white] Funcionamiento
    *********[#Turquoise]: Corriente eléctrica 
    atraviesa a la bobina;
    **********[#white]: se genera un campo
     magnetico;
    ***********[#Turquoise]: El campo magnetico atrae
     el brazo metalicó;
    ************[#white]: El brazo metalicó une 
    a los contactos;
    *************[#Turquoise]: con eso se cierra
     el circuito;
    **[#Turquoise] en 1904
    ***[#white] John Ambrose Fleming
    ****_ desarrollo
    *****[#Turquoise] componente electrico
    ******[#white] sustituyó a los Relés
    *******_ llamado
    ********[#white] Valcula Termoiónica
    *********_ trabaja por
    **********[#Turquoise] emisión termoiónica
    ***********[#white]: Permite el flujo de
    corriente en una sola 
    dirección;
    ************_ conocido como
    *************[#Turquoise] diodo
    *********_ Esta formado por
    **********[#Turquoise] Bulbo de cristal
    **********[#Turquoise] Filamento
    **********[#Turquoise] Dos electrodos
    ***********[#white] ánodo
    ***********[#white] cátodo
    *********[#Turquoise] para 1940
    **********[#white]: las valvulas se volvieron mas
     accesibles y se llamaron tubos 
     de vació;
    ***********[#Turquoise]: su primer uso fue
     en la COLOSSUS MARK 1;
    ************[#white] Diseñada por Tomy Flowers
    ************[#white] tenía 1600 tubos de vació
    ************[#white] Fue la primera computadora electronica programable
    ************[#white] En total se crearon 10 COLOSSUS
    ***********[#Turquoise]: Luego de COLOSSUS 
    se creo ENIAC;
    ************[#white]: Primera computadora eléctronica
     programable de proposito general;
    ************[#white] Construida en 1946
    *************[#Turquoise]: En la universidad
     de Pensilvania;
    **************_ Diseñada por
    ***************[#white]: John Mauchly
    J Presper Eckert;
    ************[#white]: Podia realizar 5000 sumas
     de 10 digitos por segundo;
     **[#Turquoise] en 1906
     ***[#white] lee de Forest
     ****[#Turquoise]: Averiguó un tercer electrodo 
     en la valvula termoiónica;
     *****_ llamado
     ******[#white] Electrodo de control
     *******[#Turquoise]: Estaba situado entre 
     el ánodo y el cátodo;
@endmindmap
```
## Mapa de Arquitectura de Von Neumann y Harvard

```plantuml
@startmindmap
*[#Red] ARQUITECTURA DE COMPUTADORAS
**[#Red] Ley de Moore
***[#white]: Establece que la velocidad del procesador de las 
computadoras se duplica cada doce meses;
****[#Red] Electronica
*****[#white]: Número de transistores por 
chip se duplica cada año;
*****[#white]: costo de chip permanece 
sin cambios;
****[#Red] Performance
*****[#white] se incrementa la velocidad del procesador
*****[#white] Se incrementa la capacidad de memoria
*****[#white]: la velocidad de memoria corre detras
 de la velocidad del procesador;
**[#Red]: Funcionamiento de
 una computadora;
***_ antes
****[#white] habia sistemas cableados
*****_ tenia
******[#Red] Entrada de datos
******[#Red] Secuencia
******[#Red] Resultados
*****[#Red]: Se programaba mediante
 Hardware;
***_ Ahora
****_ se tiene
*****[#White] Entrada de datos
*****[#white] secuencia de funciones
******[#Red] Interprete de instrucciones
******[#Red] Señales de control
*****[#white] Resultados
****[#white]: se programa mediante
 software;
**[#Red] ARQUITECTURAS
***[#white] Von Neumann
****_ Modelo descrito por 
*****[#Red] John Von Neumann
******_ con este concepto
*******[#white]: Surge el concepto de
programa almacenado;
******[#white] En el año de 1945
*******[#Red]: Describió una Arquitectura para
 un computador digital electronico;
********[#white]: la separación de la memoria
 y la CPU acarrea un problema;
*********_ llamado
**********[#Red] Neumann Bottleneck
***********_ debido a
************[#white]: que la cantidad de datos que pasa
 entre esos elementos difiere
 mucho en tiempo con sus velocidades;
********_ Partes que contiene
*********[#white] Unidad Central de procesamiento
**********[#Red] Unidad de control(UC)
**********[#Red] Unidad Aritmetica Logica(AUL)
**********[#Red] Registros
*********[#white] Memoria principal
**********_ puede almacenar
***********[#Red] Datos como instrucciones
*********[#white] Sistema de entrada/salida
********[#White]: Todos los componentes se comunican
 a traves de un sistema de Buses;
*********[#Red] BUS
**********_ ¿Que es?
***********[#White]: Dispositivo en común entre
 dos o mas dispositivos;
************_ tipos
*************[#Red] Bus de Datos
*************[#Red] Bus de direcciones
*************[#Red] Bus de control
*********[#white] Proposito
**********[#Red]: Reducir la cantidad de conexiones 
entre la CPU y sus sistemas;
*********[#white] Objetivo
**********[#Red]: Evitar que las señales transmitidas por dos dispositivos
 al mismo tiempo o se distorsionen y se 
 pierda información;
****[#Red] Instrucciones
*****[#White] Ciclo de una computadora
******[#Red] Ejecutar programas
*******_ los programas
********[#white]: Se encuentran localizados
 en memoria;
*********_ consisten
**********[#Red] de instrucciones
***********_ ejecutadas
************[#white] por el CPU
*************_ por medio de
**************[#Red]: un ciclo llamado ciclo
 de instrucción;
***********_ consisten
************[#white] de secuencias 1 y 0, binarias
*************_ llamadas
**************[#Red] Codigo maquina
***************[#white]: no son legibles
 para las personas;
****************[#Red]: Por eso se usan lenguajes
 como ensamblador;
****[#Red] Ciclo de ejecución
*****[#white]: UC obtiene la proxima 
instruccion de memoria;
******_ dejando
*******[#Red]: Información en el registro
 IR-fetch de instrucción;
********[#white] Se incrementa el PC
*********[#Red]: La instrucción es codificada
 a un lenguaje atendible 
 para la ALU;
**********[#white]: Obtiene de memoria los
 operando requeridos por 
 la instrucción;
***********[#Red]: La ALU ejecuta y deja
 los resultados en registros;
************[#white] Volver paso 1
***[#white] Harvard
****[#Red]: se refería a las arquitecturas 
de computo que utilizaban dispositivcos 
de almacenimiento;
*****_ separados
******[#white] Para Instrucciones
******[#white] Para datos
****_ costa de
*****_ partes
******[#Red] CPU(Unidad central de procesamiento)
*******[#white] Unidad de control(UC)
*******[#white] Unidad Aritmética logica(ALU)
*******[#white] Registros
******[#Red] Memoria principal
*******[#white] almacena datos como instrucciones
******[#Red] Sistema de Entrada/Salida
****[#Red] MEMORIAS
*****[#white]: Fabricar memorias mas 
rapidas tiene un alto
 precio;
******[#Red] Solución Harvard
*******[#white] Las instrucciones y datos
********[#Red] se almacenan en cachés
*********[#white]: separados para
 mejorar el rendimiento;
**********_ inconveniente
***********[#Red]: Divide la cantidad
de Caché entre los dos;
*****[#white] De instrucciones
******[#Red]: Se almacenan las instrucciones
 del programa que ejecutará
  el microcontrolador;
*******[#white]: El tamaño de las palabras
 de la memoria se adapta al 
 numero de BHS;
********_ se implementa
*********[#Red] Usando memorias no volatiles
*****[#white] De datos
******[#Red]: se almacenan los datos 
utilizados por los programas;
*******_ los datos
********[#white] varian continuamente
*********[#Red] utiliza memoria RAM
**********_ por ella
***********[#white]: Se realizan operaciones
 de lectura y escritura;
****[#Red] Se utiliza en microcontroladores
*****_ ¿Donde se utilizan?
******[#white]: En desarrollo de productos o sistemas
 de proposito específico;
@endmindmap
```

## Mapa de Basura Electronica
```plantuml
@startmindmap
*[#Green] BASURA ELECTRONICA
**[#white] Centros de reciclaje
***[#Green] E-END
****[#white]: Ubicado en el estado
 de Maryland, EUA;
*****_ Elementos que extraen
******[#Green] Oro
******[#Green] Cobre
******[#Green] Cobalto
******[#Green] Plata
******_ en
*******[#white]: -terminales
-circuitos
-procesadores;
******[#Green] Datos
*******[#white]: los discos duros o celulares pueden 
ser reparados con fines criminales;
********_ en e-end tienen
*********[#Green]: Un aparato con imames gigantes que 
eliminan los datos guardados
 en los aparatos;
***[#Green] PERU GREEN RECYCLING
****[#white] Ubicado en Peru
*****_ Proceso
******[#Green] 1-Recolección
*******[#Green] 2-Selección por categoría
********[#Green] 3-Se desarman
****_: a los residuos
 peligrosos;
*****[#white]: se les da un tratamiento 
diferente a los demas;
***[#Green] TECHEMET
****[#white] Ubicado en México
*****_ tienen un catalogo
******[#Green] de tarjetas electronicas
*******_ en la cual
********[#white] se clasifica todo material que llega
****[#white] Anualmente se desechan en México
*****[#Green]: entre 150 y 180 mil 
toneladas de basura eléctronica;
****[#white] Proceso
*****[#Green] 1- Recepción de material eléctrico
******[#Green] 2- se clasifica y se pesa
*******[#Green] 3- Desarmarlo y clasificarlo
********[#Green] 4- Segregación y limpieza de las piezas
*********[#Green] 5- Se almacenan segun su clasificación
**********[#Green] 6- se exporta
***[#Green] SERVICIOS ECOLOGICOS
****[#white]: Ubicados en Ciudad
 Colón, Costa Rica;
*****[#Green]: Se enfoca mas a 
la parte industrial;
******_ Industria
*******[#white] medica
*******[#white] Automotriz
*******[#white] Eléctronica, etc...
*****[#Green] Proceso
******[#white]: 1- Desarmar por completo
 cada objeto;
*******[#white]: 2-Separación por tipo
 de material;
***[#Green] REMSA
****[#white] Ubicado en México
*****[#Green] Principal Proceso
******[#white]: Volver util los vidrios de
 televisores y computadoras;
*****[#Green] Proceso
******[#white] 1- Desarman cada objeto
*******[#white] 2- Se clasifican segun su tipo
********[#white]: 3- Se trituran para despues elaborar
 distintos productos con 
 la materia prima obtenida;
*****[#Green]: Extraen 4 materias
 primas basicas;
******[#white] Plastico
******[#white] Vidrio
******[#white] Metales ferrosos
******[#white] Electronica
**[#white]: En ocaciones es exportada
 de manera ilegal;
***_ a paises como
****[#Green] China
****[#Green] India
****[#Green] México
***[#Green] ¿Por que existe un mercado ilegal?
****[#white]: Por que el reciclaje de estas 
es un proceso costoso y delicado;
****[#white]: En los paises de exportación ilegal
 usan metodos primitivos que lo 
 hacen un procesio mas barato;
*****_ desventaja
******[#Green] se expanden vapores toxicos
******[#Green] se contamina el aire
******[#Green] se contamina el agua
******[#Green] Provocan graves problemas de salud
**_ Se le conoce mundialmente como
***[#white] RAEE
****_ se define
*****[#Green]: Como Residuos de Aparatos 
Eléctricos y Eléctronicos;
@endmindmap
```
